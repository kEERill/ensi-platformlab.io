module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Техническая документация',
      items: [
        'tech/info',
        'tech/stack',
        'tech/back',
        'tech/WSL2',
        'tech/FrontReactQuery',
        'tech/infrastructure',
        'tech/EnsiStorage',
        'tech/adminGui',
        'tech/MozillaSops',
        'tech/FrontAdminGui',
        'tech/InitialEvent',
        'tech/UX',
        'tech/feature-branch',
      ],
    },
    {
      type: 'category',
      label: 'Guidelines',
      items: [
        'guid/info',
        'guid/GitlabPermissions',
        'guid/autotests',
        'guid/kafka',
        'guid/database',
        'guid/api',
        'guid/PHPCodeStyle',
        'guid/LaravelCodeStyle',
        'guid/CodeReview',
        'guid/regulations',
      ]
    },
    {
      type: 'category',
      label: 'Analytics: инструменты',
      items: [
        'analytics/info',
        'analytics/classification',
        'analytics/UserStories',
        'analytics/database',
        'analytics/sql',
        {
          type: 'category',
          label: 'Диаграммы',
          items: [
            'analytics/diagrams/info',
            'analytics/diagrams/BPMN',
            {
              type: 'category',
              label: 'UML',
              items: [
                'analytics/diagrams/uml/info',
                'analytics/diagrams/uml/activityDiagram',
                'analytics/diagrams/uml/classDiagram',
                'analytics/diagrams/uml/useCaseDiagram',
                'analytics/diagrams/uml/statechartDiagram',
                'analytics/diagrams/uml/sequenceDiagram',
              ]
            },
            'analytics/diagrams/ERD',
          ]
        },
        'analytics/api',
        'analytics/json',
        'analytics/integration',
        'analytics/protocols',
        'analytics/analytics',
        'analytics/blog',
        'analytics/metodology',
      ]
    },
    {
      type: 'category',
      label: 'Ecom Карта процессов',
      items: [
        'maps/info',
        'maps/orders',
        'maps/content',
      ]
    },
    {
      type: 'category',
      label: 'Onboarding',
      items: [
        'onboarding/info',
        'onboarding/analytics',
        'onboarding/backend',
        'onboarding/frontend',
        'onboarding/testers',
        'onboarding/UX',
      ]
    },
    'ensiDomainTeams',
    'functionalTask',
  ],
};
